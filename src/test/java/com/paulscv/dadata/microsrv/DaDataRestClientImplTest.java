package com.paulscv.dadata.microsrv;

import com.fasterxml.jackson.databind.node.ArrayNode;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;

public class DaDataRestClientImplTest {

    @Test
    public void doRequestTest() throws Exception {
        DaDataRestClient daDataClient = new DaDataRestClientImpl("short", "bank", "сбер", 2);
        Assert.assertNotNull("Could not create daDataClient !",daDataClient);
        HttpStatus queryStatus = daDataClient.doRequest();
        Assert.assertNotNull("Query status is null !", queryStatus);
        Assert.assertEquals("Error on DaDataRequest", 200, queryStatus.value());
    }

    @Test
    public void getResultTest() throws Exception {
        ArrayNode result;
        DaDataRestClient daDataClient = new DaDataRestClientImpl("short", "bank", "сбер", 2);
        HttpStatus queryStatus = daDataClient.doRequest();
        result = (ArrayNode)daDataClient.getResult();
        Assert.assertEquals("Expected Json array of size 2!", 2, result.size());
    }

}