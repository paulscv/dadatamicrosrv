package com.paulscv.dadata.microsrv;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.junit.Assert.fail;

public class DaDataBodyTest {
    DaDataBody body = new DaDataBody("bank", 2);

 //   @Rule
 //   public ExpectedException exception = ExpectedException.none();

    @Test
    public void toStringTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
    //    objectMapper.enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY);
    //    exception.expect(JsonProcessingException.class);
     //   exception.expectMessage("Json parse error in DaData request body!!!" + body.toString());
        try {
            objectMapper.readTree(body.toString());
        }
        catch (JsonParseException e){
           fail("Json parse error in DaData request body!!! " + body.toString());
        }
    }

}