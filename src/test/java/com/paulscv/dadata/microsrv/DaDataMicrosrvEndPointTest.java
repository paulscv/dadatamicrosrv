package com.paulscv.dadata.microsrv;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DaDataMicrosrvEndPointTest {

    private HttpHeaders headers;
    RestTemplate restTemplate = new RestTemplate();
    private final String requestMapping = "http://localhost:57217/api/suggest/query/short/bank/сбер/2";


    @Before
    public void initTests() {
        headers = new HttpHeaders(); //использовать именно из org.springframework.http.HttpHeaders
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        System.out.println("Integration Tests are beginning...");
    }

    @Ignore
    @Test
    public void getDaDataSuggArrayTest() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(requestMapping, HttpMethod.GET, entity, String.class);
        Assert.assertEquals("Service error!!!", 200, response.getStatusCodeValue());
    }
}