package com.paulscv.dadata.microsrv;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.http.HttpStatus;

public interface DaDataRestClient {
     HttpStatus doRequest();
     JsonNode getResult();
}
