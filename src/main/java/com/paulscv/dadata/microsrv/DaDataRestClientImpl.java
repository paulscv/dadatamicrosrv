package com.paulscv.dadata.microsrv;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

public class DaDataRestClientImpl implements  DaDataRestClient{

    private static final Logger logger = LogManager.getLogger(DaDataRestClientImpl.class);
    RestTemplate restTemplate = new RestTemplate();

    DaDataHeader header;
    DaDataUrl url;
    DaDataBody body;
    String resType; //тип ответа - полный или короткий

    ResponseEntity<String> response; //ответ от Dadata.ru
    JsonNode suggestions;
    HttpStatus respCode; //код ответа DAdata.ru
    ArrayNode result; //

    /**
     * Вариант конструктора, куда объекты Url, Header, Body строятся на основе данных из запроса
     * @param resType
     * @param obj
     * @param query
     * @param cnt
     */
    public DaDataRestClientImpl(String resType, String obj, String query, int cnt) {
        if (this.url == null) {this.url = new DaDataUrl(obj);}
        if (this.body == null) {this.body = new DaDataBody(query, cnt);}
        if (this.header == null) {this.header = new DaDataHeader();}
        this.resType = resType;
    }

    /**
     * Вариант конструктора, куда перадаются уже готовые объекты Url, Header, Body
     * @param resType
     * @param header
     * @param url
     * @param body
     */
    public DaDataRestClientImpl(String resType, DaDataHeader header, DaDataUrl url, DaDataBody body) {
        this.url = url;
        this.body = body;
        this.header = header;
        this.resType = resType;
    }

    public DaDataRestClientImpl() {
        this("short", "", "",0);
    }

    /**
     * ариант конструктора, куда объекты Url, Header, Body строятся на основе данных из запроса
     * @param resType
     * @param obj
     * @param query
     * @param cnt
     */
    public void setDaDataRestClientParams(String resType, String obj, String query, int cnt) {
        if (this.url == null) {this.url = new DaDataUrl(obj);}
        if (this.body == null) {this.body = new DaDataBody(query, cnt);}
        if (this.header == null) {this.header = new DaDataHeader();}
        this.resType = resType;
    }


    /**
     * Запрашиваем DaData.ru
     * @return статус запроса
     * сами значения ответа записывается в поле result
     */
    public HttpStatus doRequest() {
        HttpEntity<String> httpEntity = new HttpEntity<String>(body.toString(), header.httpHeaders);
        try {
            response = restTemplate.exchange(url.getUrl(), HttpMethod.POST, httpEntity, String.class);
        }
        catch (RestClientException e) {
            logger.log(Level.FATAL, "Could not connect to DaData.ru! "+e.getMessage());
            response = new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        }
        finally {
        }
        respCode = response.getStatusCode();
        setResults();
        return respCode;
    }

    /**
     * Парсим полученный от Dadata ответ. Готовим результаты
     */
    private void setResults() {
        parceResponceBody();
        result = makeValueArray();
    }

    /**
     * Разбираем ответ
     */
    private void parceResponceBody() {
        if (response != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                JsonNode bodyRoot  = mapper.readTree(response.getBody());
                this.suggestions = bodyRoot.get("suggestions");
            } catch (IOException e) {
                System.err.println(e.toString());
            }
        }
    }

    /**
     * Готовим  ответ
     * @return Массив значений Values для сокращенного ответа
     */
    private ArrayNode makeValueArray() {
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode valueArray = mapper.createArrayNode(); //your outer array
        if (suggestions.isArray()) {
            int elCnt = 0;
            for (final JsonNode sugNode : suggestions) {
                if (resType.equalsIgnoreCase("short"))
                    valueArray.add(sugNode.get("value")); // если сокращенный вариант ответа, то только значения value
                else valueArray.add(sugNode);
                if ((body.getCount() > 0) ? valueArray.size() >= body.getCount() : false) break; // Записываем только количество элементов согласно cnt
            }
        }
        return valueArray;
    }


    public JsonNode getResult() {
        return result;
    }

    public HttpStatus getRespCode() {
        return respCode;
    }

    public void setBody(DaDataBody body) {
        this.body = body;
    }

    public void setHeader(DaDataHeader header) {
        this.header = header;
    }

    public void setResType(String resType) {
        this.resType = resType;
    }

    public void setRespCode(HttpStatus respCode) {
        this.respCode = respCode;
    }
}
