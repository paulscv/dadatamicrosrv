package com.paulscv.dadata.microsrv;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("/api/suggest.*"))
                .build();
    }

    @Bean
    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Suggestion-micro-service")
                .description("Micro-service to get suggestions from DaData.ru. Test work \n\r" +
                        "Микросервис для получения подсказок с Dadata.ru. Тестовое задание")
                .version("1.0")
                .termsOfServiceUrl("http://localhost:57217/swagger-ui.html")
                .license("Проверка состояния сервиса (actuator)")
                .licenseUrl("http://localhost:8500/ui/dc1/services")
                .build();
    }
}