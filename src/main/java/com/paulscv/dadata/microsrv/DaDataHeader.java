package com.paulscv.dadata.microsrv;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Arrays;

/**
 * Кдасс для формирования Http заколовка для запроса в DaData.ru
 * https://dadata.ru/api/suggest/
 */
public class DaDataHeader {
    HttpHeaders httpHeaders;

    public DaDataHeader(){
        setHttpHeaders();
    }

    public HttpHeaders getHeades() {
        return httpHeaders;
    }

    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    public void setHttpHeaders() {
        if (httpHeaders == null) { httpHeaders = new HttpHeaders();}
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        httpHeaders.set("Authorization", "Token 273e4114f03dced50cffd05828b06b0cca65830e");
    }

}
