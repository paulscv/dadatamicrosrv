package com.paulscv.dadata.microsrv;

/**
 * Класс формирования Url запроса в DaData.ru
 */

public class DaDataUrl {
    private static final String baseUrl = "http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/";
    public String url = baseUrl;

    public DaDataUrl(String obj){
        this.setUrl(obj);
    }

    public DaDataUrl() {
        this.setUrl("");
    }

    public String getUrl(){
        return url;
    }

    public void setUrl(String obj){
        this.url = baseUrl + obj;
    }

}
