package com.paulscv.dadata.microsrv;

/**
 * Класс для тела запроса в Dadata
 * вида  '{ "query": "москва хабар", "count": 10 }'
 *  https://dadata.ru/api/suggest/
 */
public class DaDataBody {
    private String query;
    private int count = 0;

    public DaDataBody(String query) {
        this.query = query;
    }

    public DaDataBody(String query, int count) {
        this.query = query;
        this.count = count;
    }

    public DaDataBody() {
        this("",0);
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return - Строку с Json-объектом тела запроса к Dadata.ru
     *
     */
    @Override
    public String toString() {
        return "{ \"query\": \"" + query + "\"" + ((count > 0) ? ", \"cnt\":" + Integer.toString(count) : "") + "}";
    }

}
