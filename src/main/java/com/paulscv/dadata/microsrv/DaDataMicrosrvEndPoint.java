package com.paulscv.dadata.microsrv;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/suggest", produces = MediaType.APPLICATION_JSON_VALUE)
public class DaDataMicrosrvEndPoint {

    /**
     * @param type - тип запроса
     *      -short: только массив строк со знавениями value
     *      -full: массив строк с Jason-объектами
     * @param obj - запрашиваемый объект
     * @param query - строка запроса
     * @param cnt - количество элементов в массиве (optional)
     * @return - строку, содержащую массив Jason объектов
     */
    @ApiOperation(value = "Gets suggestions for address, banks, emails, etc ",
            notes = "Выдает массив Json подказок для адресов, банков, ФИО и т.п. из сервиса dadata.ru/suggestions",
            response = String.class,
            httpMethod = "GET")
    @GetMapping(value = {"query/{type}/{obj}/{query}", "query/{type}/{obj}/{query}/{cnt}"})
    @ResponseStatus(HttpStatus.OK)
    public String getDaDataSuggArray(
            @ApiParam(value = "Тип запроса: full - полный (весь объект с Dadata.ru), short - только строка value", required = true)
            @PathVariable("type") String type,
            @ApiParam(value = "Объект для запроса соответственно (bank, fio, address, email, country, okved, currency, fms_unit, fns_unit, postal_office)", required = true)
            @PathVariable("obj") String obj,
            @ApiParam(value = "Текст запроса для подсказки", required = true)
            @PathVariable("query") String query,
            @ApiParam(value = "Опционально (int) - количество подсказок в массиве", required = false)
            @PathVariable(value="cnt", required = false) Integer cnt) {
        DaDataRestClient daDataClient = new DaDataRestClientImpl(type, obj, query, ((cnt == null)? 0: cnt.intValue()));
        HttpStatus queryStatus = daDataClient.doRequest();
        if (queryStatus.value() == 200) return daDataClient.getResult().toString();
        else return "{\"status\":\"" + queryStatus.toString() + "\"}";
    }


    /**
     * Проверка ответа сервера
     * @return строку с ответом
     */
    @ApiOperation(value = "For manually check service alive",
            notes = "Проверка жив ли сервер. Лучше использовать актуатор http://localhost:8500/ui/dc1/services")
    @GetMapping("/test")
    @ResponseStatus(HttpStatus.OK)
    public String getTestSrvAction() {
        return "{\"status\":\"Server alive!\"}";
    }


}
