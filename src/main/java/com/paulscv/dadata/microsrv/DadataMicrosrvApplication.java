package com.paulscv.dadata.microsrv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DadataMicrosrvApplication {


	public static void main(String[] args) {
		SpringApplication.run(DadataMicrosrvApplication.class, args);
	}

}
