# DaData Suggestions micro-service
#### Тестовое задание:            
Реализовать Спринг бут сервис для подсказок dadata.ru , включая сваггер и актуатор. 

## Reference Documentation
#### Для запуска сервера используется сервер Consul
 Загрузка: https://www.consul.io/downloads.html
 
 Запуск: consul agent 

### Compile
$ mvn spring-boot:run -q

###__Swagger doc__

http://localhost:57217/swagger-ui.html  - for localhost

####Service Doc

  Вызов сервиса: 
  
   http://*<work-server>*:57217/api/suggest/query/{type}/{obj}/{query}/{cnt}
   
      type - тип запроса
           -short: только массив строк со знавениями value
           -full: массив строк с Jason-объектами     
      obj - запрашиваемый объект
            - bank
            - address
            - fio
            - email
            и т.п.
            см. [DaData.ru] (https://dadata.ru/api/suggest/)
            
      query - строка запроса
      cnt - количество элементов в массиве
      
      Возвращает - строку, содержащую массив Jason строк,объектов
     
   curl -X GET --header 'Accept: application/json' 'http://localhost:57217/api/suggest/query/full/fns_unit/39'